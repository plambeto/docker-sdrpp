FROM debian:bullseye

RUN apt update && apt install -y libfftw3-dev libglfw3-dev libvolk2-dev libsoapysdr-dev libairspyhf-dev libiio-dev libad9361-dev librtaudio-dev libhackrf-dev libglew-dev
RUN apt install -y wget
RUN wget https://github.com/AlexandreRouma/SDRPlusPlus/releases/download/1.0.4/sdrpp_debian_bullseye_amd64.deb
RUN dpkg -i sdrpp_debian_bullseye_amd64.deb

ENTRYPOINT [ "sleep", "infinity" ]
#ENTRYPOINT [ "sdrpp", "-s" ]


